# PEC1 Un Duel d'Insults de Pirates

En el següent document podrem veure la documentació de com s'ha realitzat un joc basat en el duel de insults dintre del joc **Monkey Island**, on podem veure un exemple del joc que ens hem basat en el següent [link](https://www.youtube.com/watch?v=hc-sxN1OnPg).

## Introducció

Primer de tot, em realitat aquest joc a partir del motor de jocs Unity (en la versió 2020.2.6f1).

El joc és un duel de insults entre dos pirates, un pirata serà el jugador i l'altre l'ordinador. El duel es divideix en diversos assalts i cada assalt es torna a dividir en un insult i una resposta. Finalment el jugador que guanya és el jugador que aconsegueixi guanyar tres assalts. A l'inici del duel, es tria de manera aleatòria qui té el torn de joc. En el nostre cas, tindrem nomes tindrem dos oponents: el jugador i l'ordinador.
En cada assalt, el oponent que té el torn crida un insult. En el cas que el torn el tingui el jugador, eligirà per pantalla quin insult cridar. Per altre banda, si és l'ordinador, la frase ha de ser aleatòria.



